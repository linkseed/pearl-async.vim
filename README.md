# async.vim for Pearl

normalize async job control api for vim and neovim

## Details

- Plugin: https://github.com/prabirshrestha/async.vim
- Pearl: https://github.com/pearl-core/pearl
